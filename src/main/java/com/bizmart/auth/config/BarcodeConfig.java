/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizmart.auth.config;

import com.bizmart.auth.common.BarcodeGenerator;
import com.bizmart.auth.common.QrCodeGenerator;
import java.awt.image.BufferedImage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;

/**
 *
 * @author imamsolikhin
 */
@Configuration
@PropertySource("classpath:application.properties")
public class BarcodeConfig {

  @Bean
  public HttpMessageConverter<BufferedImage> createImageHttpMessageConverter() {
    return new BufferedImageHttpMessageConverter();
  }

  @Bean
  public BarcodeGenerator getBarcodeGenerator() {
    return new BarcodeGenerator() {};
  }

  @Bean
  public QrCodeGenerator getQrCodeGenerator() {
    return new QrCodeGenerator();
  }
}
