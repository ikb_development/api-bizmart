package com.bizmart.auth.config;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;

@Configuration
@EnableSwagger2
// @Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

  @Bean
  public Docket auth() {
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("1 DEFAULT")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.bizmart.auth.config"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
            .securityContexts(Collections.singletonList(securityContext()))
            .securitySchemes(Collections.singletonList(apiKey()));
  }

  @Bean
  public Docket master() {
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("2 MASTER")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.bizmart.auth.controller.master"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
            .securityContexts(Collections.singletonList(securityContext()))
            .securitySchemes(Collections.singletonList(apiKey()));
  }

  @Bean
  public Docket purchase() {
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("3 PURCHASE")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.bizmart.auth.controller.purchase"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
            .securityContexts(Collections.singletonList(securityContext()))
            .securitySchemes(Collections.singletonList(apiKey()));
  }

  @Bean
  public Docket sales() {
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("4 SALES")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.bizmart.auth.controller.sales"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
            .securityContexts(Collections.singletonList(securityContext()))
            .securitySchemes(Collections.singletonList(apiKey()));
  }

  @Bean
  public Docket inventory() {
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("5 INVENTORY")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.bizmart.auth.controller.inventory"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
            .securityContexts(Collections.singletonList(securityContext()))
            .securitySchemes(Collections.singletonList(apiKey()));
  }

  @Bean
  public Docket finance() {
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("6 FINANCE")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.bizmart.auth.controller.finance"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
            .securityContexts(Collections.singletonList(securityContext()))
            .securitySchemes(Collections.singletonList(apiKey()));
  }

  @Bean
  public Docket accounting() {
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("7 ACCOUNTING")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.bizmart.auth.controller.accounting"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
            .securityContexts(Collections.singletonList(securityContext()))
            .securitySchemes(Collections.singletonList(apiKey()));
  }

  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
            .title("SYSTEM GUDANG DUNIA")
            .description("JWT Authentication Service. You can find out more about JWT at [https://jwt.io/](https://jwt.io/). For this sample, you can use the `admin` or `client` users (password: admin and client respectively) to test the authorization filters. Once you have successfully logged in and obtained the token, you should click on the right top button `Authorize` and introduce it with the prefix \"Bearer \".")
            .version("1.0.0")
            .build();
  }

  private ApiKey apiKey() {
    return new ApiKey("Authorization", "Authorization", "header");
  }

  private SecurityContext securityContext() {
    return SecurityContext.builder().securityReferences(defaultAuth()).build();
  }

  private List<SecurityReference> defaultAuth() {
    AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
    AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
    authorizationScopes[0] = authorizationScope;
    return Arrays.asList(new SecurityReference("Authorization", authorizationScopes));
  }
}
