package com.bizmart.auth.config;

import com.bizmart.auth.common.BarcodeGenerator;
import com.bizmart.auth.common.BaseProgress;
import com.bizmart.auth.common.QrCodeGenerator;
import com.bizmart.auth.common.ResponsBody;
import com.bizmart.auth.dto.v1.SignIn;
import com.bizmart.auth.dto.v1.SignUp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import com.bizmart.auth.dto.v1.UserResponseDTO;
import com.bizmart.auth.exception.v1.CustomException;
import com.bizmart.auth.security.v1.User;
import com.bizmart.auth.service.v1.UserService;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.UUID;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.modelmapper.ModelMapper;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

@RestController
@RequestMapping("oauth/v1")
@Api(tags = "Auth")
public class AuthController extends BaseProgress {

  @Autowired
  private UserService userService;

  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  ServletContext servletContext;

  private final TemplateEngine templateEngine;

  public AuthController(TemplateEngine templateEngine) {
    this.templateEngine = templateEngine;
  }

  @PostMapping("/signin")
  @ApiOperation(value = "${comments.signin}")
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 422, message = "Invalid username/password supplied")})
  public String login(@ApiParam("Signin User") @RequestBody SignIn model) {
    return userService.signin(model.getUsername(), model.getPassword()
    );
  }

  @PostMapping("/signup")
  @ApiOperation(value = "${comments.signup}")
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 403, message = "Access denied"),
    @ApiResponse(code = 422, message = "Username is already in use")})
  public String signup(@ApiParam("Signup User") @RequestBody SignUp user) {
    return userService.signup(modelMapper.map(user, User.class));
  }

  @DeleteMapping(value = "/{username}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @ApiOperation(value = "${comments.delete}", authorizations = {
    @Authorization(value = "Authorization")})
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 403, message = "Access denied"),
    @ApiResponse(code = 404, message = "The user doesn't exist"),
    @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
  public String delete(@ApiParam("Username") @PathVariable String username) {
    userService.delete(username);
    return username;
  }

  @GetMapping(value = "/{username}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @ApiOperation(value = "${comments.search}", response = UserResponseDTO.class, authorizations = {
    @Authorization(value = "Authorization")})
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 403, message = "Access denied"),
    @ApiResponse(code = 404, message = "The user doesn't exist"),
    @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
  public UserResponseDTO search(@ApiParam("Username") @PathVariable String username) {
    return modelMapper.map(userService.search(username), UserResponseDTO.class);
  }

  @GetMapping(value = "/me")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  @ApiOperation(value = "${comments.me}", response = UserResponseDTO.class, authorizations = {
    @Authorization(value = "Authorization")})
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 403, message = "Access denied"),
    @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
  public UserResponseDTO whoami(HttpServletRequest req) {
    return modelMapper.map(userService.whoami(req), UserResponseDTO.class);
  }

  @GetMapping("/refresh")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  public String refresh(HttpServletRequest req) {
    return userService.refresh(req.getRemoteUser());
  }

  @GetMapping("/progress")
  public SseEmitter eventEmitter() throws IOException {
    SseEmitter sseEmitter = new SseEmitter(Long.MAX_VALUE);
    UUID guid = UUID.randomUUID();
    System.out.println(guid.toString());

    sseEmitters.put(guid.toString(), sseEmitter);
    sseEmitter.send(SseEmitter.event().name("GUI_ID").data(guid));
    sseEmitter.onCompletion(() -> sseEmitters.remove(guid.toString()));
    sseEmitter.onTimeout(() -> sseEmitters.remove(guid.toString()));
    return sseEmitter;
  }

  @GetMapping("/test")
  public HttpEntity<Object> test(@RequestParam("stream_id") String stream_id) throws InterruptedException {
    try {
      SseEmitter sseEmitter = sseEmitters.get(stream_id);

      int totalLines = 1000;
      int lineNumber = 1;
      int previousPercent = -1;
      while (true) {
        System.out.println(totalLines + " Lines");
        int uploadPercentage = (lineNumber * 100 / totalLines);
        if (uploadPercentage != previousPercent && uploadPercentage % 5 == 0 && uploadPercentage != 55 && uploadPercentage < 100) {
          System.out.println(uploadPercentage + "% read");
          Thread.sleep(200);
          sseEmitter.send(SseEmitter.event().name(stream_id).data("progress data " + uploadPercentage + " %"));
        }
        previousPercent = uploadPercentage;
        lineNumber++;

        if (uploadPercentage > 99) {
          break;
        }
      }

      Thread.sleep(500);
      System.out.println(previousPercent + "% read");
      sseEmitter.send(SseEmitter.event().name(stream_id).data("progress data 100 %"));
      Thread.sleep(500);
      sseEmitters.remove(stream_id);

      return new HttpEntity<>(new ResponsBody("success"));
    } catch (IOException e) {
      sseEmitters.remove(stream_id);
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @GetMapping(value = "/barcode", produces = MediaType.IMAGE_PNG_VALUE)
  public ResponseEntity<Object> generateBarcode(
          @RequestParam("type") String type,
          @RequestParam("text") String barcodeText,
          @Autowired BarcodeGenerator barcodeGenerator) {

    try {
      switch (type) {
        case "EAN13":
          // 978020137962
          return new ResponseEntity<>(barcodeGenerator.generateEAN13BarcodeImage(barcodeText), OK);
        case "UPC":
          // 12345678901
          return new ResponseEntity<>(barcodeGenerator.generateUPCBarcodeImage(barcodeText), OK);
        case "EAN128":
          // 0101234567890128TEC
          return new ResponseEntity<>(barcodeGenerator.generateEAN128BarCodeImage(barcodeText), OK);
        case "CODE128":
          // any-string
          return new ResponseEntity<>(barcodeGenerator.generateCode128BarCodeImage(barcodeText), OK);
        case "USPS":
          // 123456789
          return new ResponseEntity<>(barcodeGenerator.generateUSPSBarcodeImage(barcodeText), OK);
        case "SCC14":
          return new ResponseEntity<>(barcodeGenerator.generateSCC14ShippingCodeBarcodeImage(barcodeText), OK);
        case "CODE39":
          return new ResponseEntity<>(barcodeGenerator.generateCode39BarcodeImage(barcodeText), OK);
        case "GTIN":
          return new ResponseEntity<>(barcodeGenerator.generateGlobalTradeItemNumberBarcodeImage(barcodeText), OK);
        case "PDF417":
          return new ResponseEntity<>(barcodeGenerator.generatePDF417BarcodeImage(barcodeText), OK);
        default:
          return null;
      }
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }

  @GetMapping(value = "/qrcode", produces = MediaType.IMAGE_PNG_VALUE)
  public ResponseEntity<Object> generateQrCode(
          @RequestParam("param") String param,
          @RequestParam("text") String barcodeText,
          @Autowired final QrCodeGenerator qrCodeGenerator) {

    try {
      String encodedString = Base64.getEncoder().encodeToString(barcodeText.getBytes());
      return new ResponseEntity<>(qrCodeGenerator.generate(param + encodedString, 300, 300), OK);
//      return new ResponseEntity<>(qrCodeGenerator.generateQrCode(param+encodedString, 200, 200), OK);
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }

  @GetMapping(value = "/pdf")
  public ResponseEntity<InputStreamResource> generatePdf(HttpServletRequest request, HttpServletResponse response) {
    /* Do Business Logic*/

    Order order = OrderHelper.getOrder();

    /* Create HTML using Thymeleaf template Engine */
    WebContext context = new WebContext(request, response, servletContext);
    context.setVariable("orderEntry", order);
    String orderHtml = templateEngine.process("index", context);

    /* Setup Source and target I/O streams */
    ByteArrayOutputStream target = new ByteArrayOutputStream();
    ConverterProperties converterProperties = new ConverterProperties();
    converterProperties.setBaseUri("http://localhost:8081");
    /* Call convert method */
    HtmlConverter.convertToPdf(orderHtml, target, converterProperties);

    /* extract output as bytes */
    byte[] bytes = target.toByteArray();

    HttpHeaders headers = new HttpHeaders();
    headers.add("Content-Disposition", "inline; filename=maduhijau-verification" + order.getOrderId() + ".pdf");
    return ResponseEntity
            .ok()
            .headers(headers)
            .contentType(MediaType.APPLICATION_PDF)
            .body(new InputStreamResource(new ByteArrayInputStream(target.toByteArray())));

  }
}
