package com.bizmart.auth.repository.master.v1;

import com.bizmart.auth.model.master.v1.Company;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CompanyRepository extends JpaRepository<Company, String> {

  @Query("SELECT m FROM Company m WHERE m.code = :code ")
  Company findByCode(@Param("code") String code);

  @Query("SELECT m FROM Company m WHERE "
          + "    m.code LIKE %:code% "
          + " OR m.name LIKE %:name% "
          + " OR m.activeStatus = :status "
          + " ")
  List<Company> findAllBySearch(@Param("code") String code, @Param("name") String name, @Param("status") boolean status);

  @Transactional
  void deleteByCode(String code);
}
