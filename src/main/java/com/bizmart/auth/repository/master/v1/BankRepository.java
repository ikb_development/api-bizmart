package com.bizmart.auth.repository.master.v1;

import com.bizmart.auth.model.master.v1.Bank;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BankRepository extends JpaRepository<Bank, String> {

  @Query("SELECT m FROM Bank m WHERE m.code = :code ")
  Bank findByCode(@Param("code") String code);

  @Query("SELECT m FROM Bank m WHERE "
          + "    m.code LIKE %:code% "
          + " OR m.name LIKE %:name% "
          + " OR m.activeStatus = :status "
          + " ")
  List<Bank> findAllBySearch(@Param("code") String code, @Param("name") String name, @Param("status") boolean status);

  @Transactional
  void deleteByCode(String code);
}
