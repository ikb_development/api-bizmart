package com.bizmart.auth.repository.v1;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bizmart.auth.security.v1.User;

public interface UserRepository extends JpaRepository<User, Integer> {

  boolean existsByUsername(String username);

  User findByUsername(String username);

  @Transactional
  void deleteByUsername(String username);

}
