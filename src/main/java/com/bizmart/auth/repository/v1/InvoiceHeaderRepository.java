package com.bizmart.auth.repository.v1;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bizmart.auth.model.v1.InvoiceHeader;

public interface InvoiceHeaderRepository extends JpaRepository<InvoiceHeader, Integer> {
  InvoiceHeader findByCode(String code);
}
