package com.bizmart.auth.repository.v1;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bizmart.auth.model.v1.InvoiceDetail;
import java.util.List;

public interface InvoiceDetailRepository extends JpaRepository<InvoiceDetail, Integer> {
  List<InvoiceDetail> findByHeaderCode(String code);
}
