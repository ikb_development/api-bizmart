package com.bizmart.auth.controller.master.v1;

import com.bizmart.auth.common.ResponsBody;
import com.bizmart.auth.dto.v1.Searching;
import com.bizmart.auth.exception.v1.CustomException;
import com.bizmart.auth.model.master.v1.Bank;
import com.bizmart.auth.service.master.v1.BankService;
import com.poiji.bind.Poiji;
import com.poiji.exception.PoijiExcelType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin
@RestController
@RequestMapping("/bank/v1")
@Api(tags = "Bizmart Bank")
public class BankController {

  @Autowired
  private BankService service;

  @GetMapping(value = "{code}")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  public HttpEntity<Object> data(@RequestParam String code) {
    try {
      Bank model = service.data(code);
      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "search")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  public HttpEntity<Object> list(@ApiParam("Searching master") @RequestBody Searching dto) {
    try {
      List<Bank> model = service.list(dto.getCode(), dto.getName(), dto.isStatus());
      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "save")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  public HttpEntity<Object> save(@ApiParam("Save master") @RequestBody Bank model) {
    try {
      Bank data = service.save(model);
      return new HttpEntity<>(new ResponsBody(data));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "update")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  public HttpEntity<Object> update(@ApiParam("Update master") @RequestBody Bank model) {
    try {
      Bank data = service.update(model);
      return new HttpEntity<>(new ResponsBody(data));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @DeleteMapping(value = "{code}")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  public HttpEntity<Object> delete(@RequestParam String code) {
    try {
      Bank model = service.delete(code);
      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "import")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  public HttpEntity<Object> imports(@RequestParam MultipartFile file) {
    try {
      List<Bank> model = Poiji.fromExcel(file.getInputStream(), PoijiExcelType.XLS, Bank.class);
      for (Bank bank : model) {
        service.save(bank);
      }
      return new HttpEntity<>(new ResponsBody(model));
    } catch (IOException e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "export/xls")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  public HttpEntity<ByteArrayResource> exportXlsx(@ApiParam("Searching master") @RequestBody Searching dto) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    XSSFWorkbook workbook = new XSSFWorkbook();
    try {
      List<Bank> list = service.list(dto.getCode(), dto.getName(), dto.isStatus());

      HttpHeaders header = new HttpHeaders();
      header.setContentType(new MediaType("application", "force-download"));
      header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=export-bank.xlsx");
      workbook.write(stream);
      workbook.close();

      return new ResponseEntity<>(new ByteArrayResource(stream.toByteArray()), header, HttpStatus.CREATED);
    } catch (IOException e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "export/pdf")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  public HttpEntity<ByteArrayResource> exportPdf(@ApiParam("Searching master") @RequestBody Searching dto) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    try {
      List<Bank> list = service.list(dto.getCode(), dto.getName(), dto.isStatus());

      HttpHeaders header = new HttpHeaders();
      header.setContentType(new MediaType("application", "force-download"));
      header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=export-bank.pdf");

      return new ResponseEntity<>(new ByteArrayResource(stream.toByteArray()), header, HttpStatus.CREATED);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }
}
