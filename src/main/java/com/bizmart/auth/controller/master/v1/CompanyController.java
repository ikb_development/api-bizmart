package com.bizmart.auth.controller.master.v1;

import com.bizmart.auth.common.BaseProgress;
import com.bizmart.auth.common.ResponsBody;
import com.bizmart.auth.dto.v1.Searching;
import com.bizmart.auth.exception.v1.CustomException;
import com.bizmart.auth.model.master.v1.Company;
import com.bizmart.auth.service.master.v1.CompanyService;
import com.poiji.bind.Poiji;
import com.poiji.exception.PoijiExcelType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/company/v1")
@Api(tags = "Bizmart Company")
public class CompanyController extends BaseProgress{

  @Autowired
  private CompanyService service;

  @GetMapping(value = "{code}")
  public HttpEntity<Object> data(@RequestParam String code) {
    try {
      Company model = service.data(code);
      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "search")
  public HttpEntity<Object> list(@ApiParam("Searching master") @RequestBody Searching dto) {
    try {
      List<Company> model = service.list(dto.getCode(), dto.getName(), dto.isStatus());
      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "save")
  public HttpEntity<Object> save(@ApiParam("Save master") @RequestBody Company model) {
    try {
      Company data = service.save(model);
      return new HttpEntity<>(new ResponsBody(data));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "update")
  public HttpEntity<Object> update(@ApiParam("Update master") @RequestBody Company model) {
    try {
      Company data = service.update(model);
      return new HttpEntity<>(new ResponsBody(data));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @DeleteMapping(value = "{code}")
  public HttpEntity<Object> delete(@RequestParam String code) {
    try {
      Company model = service.delete(code);
      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "import")
  public HttpEntity<Object> imports(@RequestParam MultipartFile file) {
    try {
      List<Company> model = Poiji.fromExcel(file.getInputStream(), PoijiExcelType.XLS, Company.class);
      for (Company company : model) {
        service.save(company);
      }
      return new HttpEntity<>(new ResponsBody(model));
    } catch (IOException e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "export/xls")
  public HttpEntity<ByteArrayResource> exportXlsx(@ApiParam("Searching master") @RequestBody Searching dto) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    XSSFWorkbook workbook = new XSSFWorkbook();
    try {
      List<Company> list = service.list(dto.getCode(), dto.getName(), dto.isStatus());

      HttpHeaders header = new HttpHeaders();
      header.setContentType(new MediaType("application", "force-download"));
      header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=export-company.xlsx");
      workbook.write(stream);
      workbook.close();

      return new ResponseEntity<>(new ByteArrayResource(stream.toByteArray()), header, HttpStatus.CREATED);
    } catch (IOException e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "export/pdf")
  public HttpEntity<ByteArrayResource> exportPdf(@ApiParam("Searching master") @RequestBody Searching dto) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    try {
      List<Company> list = service.list(dto.getCode(), dto.getName(), dto.isStatus());

      HttpHeaders header = new HttpHeaders();
      header.setContentType(new MediaType("application", "force-download"));
      header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=export-company.pdf");

      return new ResponseEntity<>(new ByteArrayResource(stream.toByteArray()), header, HttpStatus.CREATED);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }
}
