/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizmart.auth.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author imamsolikhin
 */
public class Utility {

  public static void print(Object obj) {
    System.err.println(obj.toString());
  }

  public static Map<String, Object> jsonToMap(Object json) throws JSONException {

    if (json instanceof JSONObject) {
      return _jsonToMap_((JSONObject) json);
    } else if (json instanceof String) {
      JSONObject jsonObject = new JSONObject((String) json);
      return _jsonToMap_(jsonObject);
    }
    return null;
  }

  private static Map<String, Object> _jsonToMap_(JSONObject json) throws JSONException {
    Map<String, Object> retMap = new HashMap<String, Object>();

    if (json != JSONObject.NULL) {
      retMap = toMap(json);
    }
    return retMap;
  }

  private static Map<String, Object> toMap(JSONObject object) throws JSONException {
    Map<String, Object> map = new HashMap<String, Object>();

    Iterator<String> keysItr = object.keys();
    while (keysItr.hasNext()) {
      String key = keysItr.next();
      Object value = object.get(key);

      if (value instanceof JSONArray) {
        value = toList((JSONArray) value);
      } else if (value instanceof JSONObject) {
        value = toMap((JSONObject) value);
      }
      map.put(key, value);
    }
    return map;
  }

  public static HashMap<String, String> jsonToHasMap(JSONObject json) throws JSONException {
    HashMap<String, String> map = new HashMap<>();
    try {
      Iterator<String> iterator = json.keys();

      while (iterator.hasNext()) {
        String key = iterator.next();
        String value = json.getString(key);
        map.put(key, value);
      }

      return map;
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return null;
  }
  
  public static HashMap<String, String> fileJsonToMap(MultipartFile file) throws JSONException, IOException {
    HashMap<String, String> map = new HashMap<>();
    try {
      map = new ObjectMapper().readValue(file.getInputStream(), HashMap.class);
      return map;
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static List<Object> toList(JSONArray array) throws JSONException {
    List<Object> list = new ArrayList<Object>();
    for (int i = 0; i < array.length(); i++) {
      Object value = array.get(i);
      if (value instanceof JSONArray) {
        value = toList((JSONArray) value);
      } else if (value instanceof JSONObject) {
        value = toMap((JSONObject) value);
      }
      list.add(value);
    }
    return list;
  }
}
