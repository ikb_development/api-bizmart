/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizmart.auth.common;

import com.bizmart.auth.exception.v1.CustomException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.springframework.http.HttpStatus;

/**
 *
 * @author imamsolikhin
 */
public class FtpClient {

  private boolean is_ssh = false;
  private String server = "127.0.0.1";
  private int port = 21;
  private String user = "username";
  private String password = "password";
  private FTPClient ftp = new FTPClient();

  // constructor
  public FtpClient(String server, int port, String user, String pass) {
    this.server = server;
    this.port = port;
    this.user = user;
    this.password = pass;
  }

  // constructor
  public FtpClient(String server, int port, String user, String pass, boolean is_ssh) {
    this.server = server;
    this.port = port;
    this.user = user;
    this.password = pass;
    this.is_ssh = is_ssh;
  }

  public void open() {
    try {
      this.ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
      if (is_ssh) {
        this.ftp = new FTPSClient();
      }
      this.ftp.connect(this.server, this.port);
      int reply = this.ftp.getReplyCode();

      if (!FTPReply.isPositiveCompletion(reply)) {
        this.ftp.disconnect();
        throw new CustomException("Conetion Loss on ftp://" + this.server + "!", HttpStatus.BAD_GATEWAY);
      }

      this.ftp.login(this.user, this.password);
      if (!FTPReply.isPositiveCompletion(this.ftp.getReplyCode())) {
        this.ftp.disconnect();
        throw new CustomException("login fail", HttpStatus.FORBIDDEN);
      }
      List<String> files = listFiles();
      files.forEach(f -> {
        System.out.println(f);
      });
    } catch (IOException e) {
      throw new CustomException(e.getMessage(), HttpStatus.FORBIDDEN);
    }
  }

  public void close() {
    try {
      this.ftp.disconnect();
    } catch (IOException e) {
      throw new CustomException(e.getMessage(), HttpStatus.FORBIDDEN);
    }
  }

  public List<String> listFiles() {
    try {
      this.ftp.setFileType(FTP.ASCII_FILE_TYPE);
      FTPFile[] files = this.ftp.listFiles();
      return printFileDetails(files);
    } catch (IOException e) {
      throw new CustomException(e.getMessage(), HttpStatus.FORBIDDEN);
    }
  }

  public List<String> listFiles(String path) {
    try {
      this.ftp.setFileType(FTP.ASCII_FILE_TYPE);
      FTPFile[] files = this.ftp.listFiles(path);
      return printFileDetails(files);
    } catch (IOException e) {
      throw new CustomException(e.getMessage(), HttpStatus.FORBIDDEN);
    }
  }

  private static List<String> printFileDetails(FTPFile[] files) {
    DateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    List<String> result = new ArrayList<String>();
    for (FTPFile file : files) {
      result.add(file.getName());
    }
    return result;
  }

  private static void printNames(String files[]) {
    if (files != null && files.length > 0) {
      for (String aFile : files) {
        System.out.println(aFile);
      }
    }
  }

  private static void showServerReply(FTPClient ftpClient) {
    String[] replies = ftpClient.getReplyStrings();
    if (replies != null && replies.length > 0) {
      for (String aReply : replies) {
        System.out.println("SERVER: " + aReply);
      }
    }
  }
}
