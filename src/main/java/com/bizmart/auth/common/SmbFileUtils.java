/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizmart.auth.common;

import com.bizmart.auth.exception.v1.CustomException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;
import org.springframework.http.HttpStatus;

/**
 *
 * @author ahmad
 */
public class SmbFileUtils {

  public static String USER_DOMAIN = "";
  public static String SHARE_DIR = "smb://10.200.51.201";
  public static String USER_ACCOUNT = "data.file";
  public static String USER_PWD = "ITCepmfs123!";
  public static String FILE_NAME = null; //Password

  public static List<String> smbList(String shareDirectory) throws Exception {
    NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(USER_DOMAIN, USER_ACCOUNT, USER_PWD);
    SmbFile smb = new SmbFile(SHARE_DIR + shareDirectory, auth);
    List<String> result = new ArrayList<String>();
    if (!smb.exists()) {
      throw new CustomException("no data!", HttpStatus.NOT_FOUND);
    }
    
    SmbFile[] files = smb.listFiles();
    for (SmbFile file : files) {
      result.add(file.getName());
    }
    return result;
  }

  public static void smbGet(String shareUrl, String localDirectory) throws Exception {
    SmbFileUtils.smbGetFile(shareUrl, localDirectory);
  }

  public static void smbGet(String shareUrl, String localDirectory, String name) throws Exception {
    SmbFileUtils.FILE_NAME = name;
    SmbFileUtils.smbGetFile(shareUrl, localDirectory);
  }

  public static void smbGetFile(String shareUrl, String localDirectory) throws Exception {
    NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(USER_DOMAIN, USER_ACCOUNT, USER_PWD);
    SmbFile remoteFile = new SmbFile(SHARE_DIR + shareUrl, auth);
    if (!remoteFile.exists()) {
      System.out.print("Shared file does not exist");
      return;
    } else {
      System.out.println("Shared file Accessed");
    }

    if (remoteFile.exists() == false) {
      remoteFile.mkdirs();
    }

    File locDir = new File(localDirectory);
    if (locDir.exists() == false) {
      locDir.mkdirs();
    }

    InputStream in = null;
    OutputStream out = null;
    try {
      File fname = new File(SHARE_DIR + shareUrl);
      if (SmbFileUtils.FILE_NAME == null) {
        SmbFileUtils.FILE_NAME = fname.getName();
      }

      File localFile = new File(localDirectory + File.separator + SmbFileUtils.FILE_NAME);
      in = new BufferedInputStream(new SmbFileInputStream(remoteFile));
      out = new BufferedOutputStream(new FileOutputStream(localFile));
      byte[] buffer = new byte[1024];
      while (in.read(buffer) != -1) {
        out.write(buffer);
        buffer = new byte[1024];
      }
      out.flush();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      out.close();
      in.close();
    }
  }

  public static void smbPut(String localDirectory, String shareUrl) throws Exception {
    File localFile = new File(localDirectory);
    SmbFileUtils.smbPutFile(localFile, shareUrl);
  }

  public static void smbPut(String localDirectory, String shareUrl, String name) throws Exception {
    SmbFileUtils.FILE_NAME = name;
    File localFile = new File(localDirectory);
    SmbFileUtils.smbPutFile(localFile, shareUrl);
  }

  public static void smbPut(File localFile, String shareUrl) throws Exception {
    SmbFileUtils.smbPutFile(localFile, shareUrl);
  }

  public static void smbPut(File localFile, String shareUrl, String name) throws Exception {
    SmbFileUtils.FILE_NAME = name;
    SmbFileUtils.smbPutFile(localFile, shareUrl);
  }

  public static void smbPutFile(File localFile, String shareDirectory) {
    InputStream in = null;
    OutputStream out = null;

    if (SmbFileUtils.FILE_NAME == null) {
      SmbFileUtils.FILE_NAME = localFile.getName();
    }

    try {
      NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(USER_DOMAIN, USER_ACCOUNT, USER_PWD);
      SmbFile remoteFolder = new SmbFile(SHARE_DIR + shareDirectory, auth);
      if (!remoteFolder.exists()) {
        remoteFolder.mkdirs();
      }

      SmbFile remoteFile = new SmbFile(SHARE_DIR + shareDirectory + File.separator + SmbFileUtils.FILE_NAME, auth);

      in = new BufferedInputStream(new FileInputStream(localFile));
      out = new BufferedOutputStream(new SmbFileOutputStream(remoteFile));
      byte[] buffer = new byte[1024];
      while (in.read(buffer) != -1) {
        out.write(buffer);
        buffer = new byte[1024];
      }
      out.flush();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        out.close();
        in.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
