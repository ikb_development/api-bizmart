/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizmart.auth.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

/**
 *
 * @author imamsolikhin
 */
@MappedSuperclass
public abstract class BaseModel {

  @JsonIgnore
  @CreatedBy
  @Column(name = "createdBy", updatable = false)
  private String createdBy = "";

  @JsonIgnore
  @CreatedDate
  @Column(name = "createdDate", updatable = false)
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date createdDate;

  @JsonIgnore
  @LastModifiedBy
  @Column(name = "updatedBy", updatable = true)
  private String updatedBy = "";

  @JsonIgnore
  @LastModifiedDate
  @Column(name = "updatedDate", updatable = true)
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date updatedDate;

  @PrePersist
  void onCreate() {
    this.createdBy = "imam";
    this.createdDate = new Date();
  }

  @PreUpdate
  void onPersist() {
    this.updatedBy = "system";
    this.updatedDate = new Date();
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public Date getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(Date updatedDate) {
    this.updatedDate = updatedDate;
  }

}
