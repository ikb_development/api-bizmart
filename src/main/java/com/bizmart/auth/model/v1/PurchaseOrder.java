    //model PO
package com.bizmart.auth.model.v1;

import com.poiji.annotation.ExcelCell;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "pur_purchase_order")
public class PurchaseOrder implements Serializable {

  @Id
  @ExcelCell(0)
  @Column(name = "Code")
  private String code = "";

  @ExcelCell(1)
  @Column(name = "TransactionDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date transactionDate = new Date();

  @ExcelCell(2)
  @Column(name = "CorporateCode")
  private String corporateCode = null;

  @ExcelCell(3)
  @Column(name = "StrategicBusinessUnitCode")
  private String strategicBusinessUnitCode = null;

  @ExcelCell(4)
  @Column(name = "DivisionCode")
  private String divisionCode = null;

  @ExcelCell(5)
  @Column(name = "PlantCode")
  private String plantCode = null;

  @ExcelCell(6)
  @Column(name = "PaymentTerm")
  private String paymentTerm = "";

  @ExcelCell(7)
  @Column(name = "currencyCode")
  private String currencyCode = "";

  @ExcelCell(8)
  @Column(name = "vendorCode")
  private String vendorCode = null;

  @ExcelCell(9)
  @Column(name = "RefNo")
  private String refNo = "";

  @ExcelCell(10)
  @Column(name = "Remark")
  private String remark = "";

  @ExcelCell(11)
  @Column(name = "TotalTransactionAmount")
  private BigDecimal totalTransactionAmount = new BigDecimal("0.00");

  @ExcelCell(12)
  @Column(name = "VatPercent")
  private BigDecimal vatPercent = new BigDecimal("0.00");

  @ExcelCell(13)
  @Column(name = "VatAmount")
  private BigDecimal vatAmount = new BigDecimal("0.00");

  @ExcelCell(14)
  @Column(name = "GrandTotalAmount")
  private BigDecimal grandTotalAmount = new BigDecimal("0.00");

  @ExcelCell(15)
  @Column(name = "createdBy")
  private String createdBy = "";

  @ExcelCell(16)
  @Column(name = "createdDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date createdDate = new Date();

  @ExcelCell(17)
  @Column(name = "updatedBy")
  private String updatedBy = "";

  @ExcelCell(18)
  @Column(name = "updatedDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date updatedDate = new Date();

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Date getTransactionDate() {
    return transactionDate;
  }

  public void setTransactionDate(Date transactionDate) {
    this.transactionDate = transactionDate;
  }

  public String getCorporateCode() {
    return corporateCode;
  }

  public void setCorporateCode(String corporateCode) {
    this.corporateCode = corporateCode;
  }

  public String getStrategicBusinessUnitCode() {
    return strategicBusinessUnitCode;
  }

  public void setStrategicBusinessUnitCode(String strategicBusinessUnitCode) {
    this.strategicBusinessUnitCode = strategicBusinessUnitCode;
  }

  public String getDivisionCode() {
    return divisionCode;
  }

  public void setDivisionCode(String divisionCode) {
    this.divisionCode = divisionCode;
  }

  public String getPlantCode() {
    return plantCode;
  }

  public void setPlantCode(String plantCode) {
    this.plantCode = plantCode;
  }

  public String getPaymentTerm() {
    return paymentTerm;
  }

  public void setPaymentTerm(String paymentTerm) {
    this.paymentTerm = paymentTerm;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getVendorCode() {
    return vendorCode;
  }

  public void setVendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
  }

  public String getRefNo() {
    return refNo;
  }

  public void setRefNo(String refNo) {
    this.refNo = refNo;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public BigDecimal getTotalTransactionAmount() {
    return totalTransactionAmount;
  }

  public void setTotalTransactionAmount(BigDecimal totalTransactionAmount) {
    this.totalTransactionAmount = totalTransactionAmount;
  }

  public BigDecimal getVatPercent() {
    return vatPercent;
  }

  public void setVatPercent(BigDecimal vatPercent) {
    this.vatPercent = vatPercent;
  }

  public BigDecimal getVatAmount() {
    return vatAmount;
  }

  public void setVatAmount(BigDecimal vatAmount) {
    this.vatAmount = vatAmount;
  }

  public BigDecimal getGrandTotalAmount() {
    return grandTotalAmount;
  }

  public void setGrandTotalAmount(BigDecimal grandTotalAmount) {
    this.grandTotalAmount = grandTotalAmount;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public Date getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(Date updatedDate) {
    this.updatedDate = updatedDate;
  }
}
