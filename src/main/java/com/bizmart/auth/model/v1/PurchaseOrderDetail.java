//PO Detail
package com.bizmart.auth.model.v1;

import com.poiji.annotation.ExcelCell;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "pur_purchase_order_detail")
public class PurchaseOrderDetail implements Serializable {

  @Id
  @ExcelCell(0)
  @Column(name = "code")
  private String code = "";

  @ExcelCell(1)
  @Column(name = "headerCode")
  private String headerCode = "";

  @ExcelCell(2)
  @Column(name = "purchaseRequestNo")
  private String purchaseRequestNo = "";

  @ExcelCell(3)
  @Column(name = "purchaseRequestDetailCode")
  private String purchaseRequestDetailCode = "";

  @ExcelCell(4)
  @Column(name = "itemCode")
  private String itemCode = "";

  @ExcelCell(5)
  @Column(name = "itemName")
  private String itemName = "";

  @ExcelCell(6)
  @Column(name = "remark")
  private String remark = "";

  @ExcelCell(7)
  @Column(name = "quantity")
  private BigDecimal quantity = new BigDecimal("0.0000");

  @ExcelCell(8)
  @Column(name = "uom")
  private String uom = "";

  @ExcelCell(9)
  @Column(name = "price")
  private BigDecimal price = new BigDecimal("0.0000");

  @ExcelCell(10)
  @Column(name = "totalAmount")
  private BigDecimal totalAmount = new BigDecimal("0.0000");

  @ExcelCell(11)
  @Column(name = "createdBy")
  private String createdBy = "";

  @ExcelCell(12)
  @Column(name = "createdDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date createdDate = new Date();

  @ExcelCell(13)
  @Column(name = "updatedBy")
  private String updatedBy = "";

  @ExcelCell(14)
  @Column(name = "updatedDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date updatedDate = new Date();

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getHeaderCode() {
    return headerCode;
  }

  public void setHeaderCode(String headerCode) {
    this.headerCode = headerCode;
  }

  public String getPurchaseRequestNo() {
    return purchaseRequestNo;
  }

  public void setPurchaseRequestNo(String purchaseRequestNo) {
    this.purchaseRequestNo = purchaseRequestNo;
  }

  public String getPurchaseRequestDetailCode() {
    return purchaseRequestDetailCode;
  }

  public void setPurchaseRequestDetailCode(String purchaseRequestDetailCode) {
    this.purchaseRequestDetailCode = purchaseRequestDetailCode;
  }

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public String getUom() {
    return uom;
  }

  public void setUom(String uom) {
    this.uom = uom;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(BigDecimal totalAmount) {
    this.totalAmount = totalAmount;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public Date getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(Date updatedDate) {
    this.updatedDate = updatedDate;
  }

}
