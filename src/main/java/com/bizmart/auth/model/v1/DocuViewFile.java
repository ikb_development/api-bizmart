/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizmart.auth.model.v1;

import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author imamsolikhin
 */
@XmlRootElement(name = "ViewFile")
@XmlAccessorType(XmlAccessType.FIELD)
public class DocuViewFile {

  private int VerID;
  private int ProfileID;
  private int FileType;

  public static Object toXML(DocuViewFile obj) {
    String rest = "";
    StringWriter sw = new StringWriter();

    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(DocuViewFile.class);

      Marshaller marshaller = jaxbContext.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
//      marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://tempuri.org");
      marshaller.marshal(obj, sw);

      rest = sw.toString();
    } catch (JAXBException e) {
      e.printStackTrace();
    }
    return rest;
  }

  public int getVerID() {
    return VerID;
  }

  public void setVerID(int VerID) {
    this.VerID = VerID;
  }

  public int getProfileID() {
    return ProfileID;
  }

  public void setProfileID(int ProfileID) {
    this.ProfileID = ProfileID;
  }

  public int getFileType() {
    return FileType;
  }

  public void setFileType(int FileType) {
    this.FileType = FileType;
  }
}
