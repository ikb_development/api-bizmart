/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizmart.auth.model.v1;

import com.poiji.annotation.ExcelCell;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author imamsolikhin
 */
@Entity
@Table(name = "mst_strategic_business_unit")
public class StrategicBusinessUnit implements Serializable {

  @Id
  @ExcelCell(0)
  @Column(name = "Code")
  private String code = "";

  @ExcelCell(1)
  @Column(name = "Name")
  private String name = "";

  @ExcelCell(2)
  @Column(name = "activeStatus")
  private boolean activeStatus = false;

  @ExcelCell(3)
  @Column(name = "createdBy")
  private String createdBy = "";

  @ExcelCell(4)
  @Column(name = "createdDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date createdDate = new Date();

  @ExcelCell(5)
  @Column(name = "inActiveBy")
  private String inActiveBy = "";

  @ExcelCell(6)
  @Column(name = "inActiveDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date inActiveDate = new Date();

  @ExcelCell(7)
  @Column(name = "updatedBy")
  private String updatedBy = "";

  @ExcelCell(8)
  @Column(name = "updatedDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date updatedDate = new Date();

  @ExcelCell(9)
  @Column(name = "remark")
  private String remark = "";

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(boolean activeStatus) {
    this.activeStatus = activeStatus;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public String getInActiveBy() {
    return inActiveBy;
  }

  public void setInActiveBy(String inActiveBy) {
    this.inActiveBy = inActiveBy;
  }

  public Date getInActiveDate() {
    return inActiveDate;
  }

  public void setInActiveDate(Date inActiveDate) {
    this.inActiveDate = inActiveDate;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public Date getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(Date updatedDate) {
    this.updatedDate = updatedDate;
  }

}
