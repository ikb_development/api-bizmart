package com.bizmart.auth.model.v1;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "_cron_invoice_detail")
public class InvoiceDetail implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(unique = false, nullable = true)
  private String headerCode;

  @Column(unique = false, nullable = true)
  private String itemName;

  @Column(unique = false, nullable = true)
  private String quantity;

  @Column(unique = false, nullable = true)
  private String amount;

  @Column(unique = false, nullable = true)
  private String subTotal;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getHeaderCode() {
    return headerCode;
  }

  public void setHeaderCode(String headerCode) {
    this.headerCode = headerCode;
  }

  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }

  public String getQuantity() {
    return quantity;
  }

  public void setQuantity(String quantity) {
    this.quantity = quantity;
  }

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public String getSubTotal() {
    return subTotal;
  }

  public void setSubTotal(String subTotal) {
    this.subTotal = subTotal;
  }
}
