package com.bizmart.auth.model.v1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "_cron_invoice_header")
public class InvoiceHeader implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(unique = false, nullable = true)
  private String code;

  @Column(unique = false, nullable = true)
  private String transactionDate;

  @Column(unique = false, nullable = true)
  private String poNo;

  @Column(unique = false, nullable = true)
  private String companyName;

  @Column(unique = false, nullable = true)
  private String currency;

  @Column(unique = false, nullable = true)
  private String tax;

  @Column(unique = false, nullable = true)
  private String amount;

  @Column(unique = false, nullable = true)
  private String baseLine;

  private @Transient
  List<InvoiceDetail> invoiceDetails = new ArrayList<>();

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getTransactionDate() {
    return transactionDate;
  }

  public void setTransactionDate(String transactionDate) {
    this.transactionDate = transactionDate;
  }

  public String getPoNo() {
    return poNo;
  }

  public void setPoNo(String poNo) {
    this.poNo = poNo;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getTax() {
    return tax;
  }

  public void setTax(String tax) {
    this.tax = tax;
  }

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public String getBaseLine() {
    return baseLine;
  }

  public void setBaseLine(String baseLine) {
    this.baseLine = baseLine;
  }

  public List<InvoiceDetail> getInvoiceDetails() {
    return invoiceDetails;
  }

  public void setInvoiceDetails(List<InvoiceDetail> invoiceDetails) {
    this.invoiceDetails = invoiceDetails;
  }

}
