package com.bizmart.auth.model.v1;

import com.poiji.annotation.ExcelCell;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "ivt_goods_received_note_item_detail")
public class GoodsReceivedNoteDetail implements Serializable {

  @Id
  @ExcelCell(0)
  @Column(name = "Code")
  private String code = "";

  @ExcelCell(1)
  @Column(name = "HeaderCode")
  private String headerCode = "";

  @ExcelCell(2)
  @Column(name = "PurchaseOrderDetailCode")
  private String purchaseOrderDetailCode = "";

  @ExcelCell(3)
  @Column(name = "ItemCode")
  private String itemCode = "";

  @ExcelCell(4)
  @Column(name = "ItemName")
  private String itemName = "";

  @ExcelCell(5)
  @Column(name = "Quantity")
  private Double quantity;

  @ExcelCell(6)
  @Column(name = "UOM")
  private String uom = "";

  @ExcelCell(7)
  @Column(name = "Remark")
  private String remark = "";

  @ExcelCell(8)
  @Column(name = "createdBy")
  private String createdBy = "";

  @ExcelCell(9)
  @Column(name = "createdDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date createdDate = new Date();

  @ExcelCell(10)
  @Column(name = "updatedBy")
  private String updatedBy = "";

  @ExcelCell(11)
  @Column(name = "updatedDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date updatedDate = new Date();

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getHeaderCode() {
    return headerCode;
  }

  public void setHeaderCode(String headerCode) {
    this.headerCode = headerCode;
  }

  public String getPurchaseOrderDetailCode() {
    return purchaseOrderDetailCode;
  }

  public void setPurchaseOrderDetailCode(String purchaseOrderDetailCode) {
    this.purchaseOrderDetailCode = purchaseOrderDetailCode;
  }

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }

  public Double getQuantity() {
    return quantity;
  }

  public void setQuantity(Double quantity) {
    this.quantity = quantity;
  }

  public String getUom() {
    return uom;
  }

  public void setUom(String uom) {
    this.uom = uom;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public Date getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(Date updatedDate) {
    this.updatedDate = updatedDate;
  }
}
