/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizmart.auth.model.v1;

import com.poiji.annotation.ExcelCell;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author imamsolikhin
 */
@Entity
@Table(name = "ivt_goods_received_note")
public class GoodsReceivedNote implements Serializable {

  @Id
  @ExcelCell(0)
  @Column(name = "Code")
  private String code = "";

  @ExcelCell(1)
  @Column(name = "TransactionDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date transactionDate = new Date();

  @ExcelCell(2)
  @Column(name = "PurchaseOrderCode")
  private String vendorPurchaseOrder = null;

  @ExcelCell(3)
  @Column(name = "VendorCode")
  private String vendorCode = "";

  @ExcelCell(4)
  @Column(name = "ReceivedBy")
  private String receivedBy = "";

  @ExcelCell(5)
  @Column(name = "VendorDo")
  private String vendorDo = "";

  @ExcelCell(6)
  @Column(name = "PoliceNo")
  private String policeNo = "";

  @ExcelCell(7)
  @Column(name = "ContainerNo")
  private String containerNo = "";

  @ExcelCell(8)
  @Column(name = "RefNo")
  private String refNo = "";

  @ExcelCell(9)
  @Column(name = "Remark")
  private String remark = "";

  @ExcelCell(10)
  @Column(name = "createdBy")
  private String createdBy = "";

  @ExcelCell(11)
  @Column(name = "createdDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date createdDate = new Date();

  @ExcelCell(12)
  @Column(name = "updatedBy")
  private String updatedBy = "";

  @ExcelCell(13)
  @Column(name = "updatedDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date updatedDate = new Date();

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Date getTransactionDate() {
    return transactionDate;
  }

  public void setTransactionDate(Date transactionDate) {
    this.transactionDate = transactionDate;
  }

  public String getVendorPurchaseOrder() {
    return vendorPurchaseOrder;
  }

  public void setVendorPurchaseOrder(String vendorPurchaseOrder) {
    this.vendorPurchaseOrder = vendorPurchaseOrder;
  }

  public String getVendorCode() {
    return vendorCode;
  }

  public void setVendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
  }

  public String getReceivedBy() {
    return receivedBy;
  }

  public void setReceivedBy(String receivedBy) {
    this.receivedBy = receivedBy;
  }

  public String getVendorDo() {
    return vendorDo;
  }

  public void setVendorDo(String vendorDo) {
    this.vendorDo = vendorDo;
  }

  public String getPoliceNo() {
    return policeNo;
  }

  public void setPoliceNo(String policeNo) {
    this.policeNo = policeNo;
  }

  public String getContainerNo() {
    return containerNo;
  }

  public void setContainerNo(String containerNo) {
    this.containerNo = containerNo;
  }

  public String getRefNo() {
    return refNo;
  }

  public void setRefNo(String refNo) {
    this.refNo = refNo;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public Date getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(Date updatedDate) {
    this.updatedDate = updatedDate;
  }
}
