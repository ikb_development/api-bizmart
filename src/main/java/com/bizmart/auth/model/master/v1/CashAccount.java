package com.bizmart.auth.model.master.v1;

import com.bizmart.auth.common.BaseModel;
import java.io.Serializable;
import com.poiji.annotation.ExcelCell;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mst_cash_account")
public class CashAccount extends BaseModel implements Serializable {

  @Id
  @ExcelCell(0)
  @Column(name = "Code")
  private String code = "";

  @ExcelCell(1)
  @Column(name = "Name")
  private String name = "";

  @ExcelCell(2)
  @Column(name = "acNo")
  private String acNo = "";

  @ExcelCell(3)
  @Column(name = "acName")
  private String acName = "";

  @ExcelCell(4)
  @Column(name = "bankName")
  private String bankName = "";

  @ExcelCell(5)
  @Column(name = "bankBranch")
  private String bankBranch = "";

  @ExcelCell(6)
  @Column(name = "bbmVoucherNo")
  private String bbmVoucherNo = "";

  @ExcelCell(7)
  @Column(name = "bbkVoucherNo")
  private String bbkVoucherNo = "";

  @ExcelCell(8)
  @Column(name = "accountNo")
  private String accountNo = null;

  @ExcelCell(9)
  @Column(name = "bbmGiroAccountNo")
  private String bbmGiroAccountNo = null;

  @ExcelCell(10)
  @Column(name = "bbkGiroAccountNo")
  private String bbkGiroAccountNo = null;

  @ExcelCell(11)
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = false;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAcNo() {
    return acNo;
  }

  public void setAcNo(String acNo) {
    this.acNo = acNo;
  }

  public String getAcName() {
    return acName;
  }

  public void setAcName(String acName) {
    this.acName = acName;
  }

  public String getBankName() {
    return bankName;
  }

  public void setBankName(String bankName) {
    this.bankName = bankName;
  }

  public String getBankBranch() {
    return bankBranch;
  }

  public void setBankBranch(String bankBranch) {
    this.bankBranch = bankBranch;
  }

  public String getBbmVoucherNo() {
    return bbmVoucherNo;
  }

  public void setBbmVoucherNo(String bbmVoucherNo) {
    this.bbmVoucherNo = bbmVoucherNo;
  }

  public String getBbkVoucherNo() {
    return bbkVoucherNo;
  }

  public void setBbkVoucherNo(String bbkVoucherNo) {
    this.bbkVoucherNo = bbkVoucherNo;
  }

  public String getAccountNo() {
    return accountNo;
  }

  public void setAccountNo(String accountNo) {
    this.accountNo = accountNo;
  }

  public String getBbmGiroAccountNo() {
    return bbmGiroAccountNo;
  }

  public void setBbmGiroAccountNo(String bbmGiroAccountNo) {
    this.bbmGiroAccountNo = bbmGiroAccountNo;
  }

  public String getBbkGiroAccountNo() {
    return bbkGiroAccountNo;
  }

  public void setBbkGiroAccountNo(String bbkGiroAccountNo) {
    this.bbkGiroAccountNo = bbkGiroAccountNo;
  }

  public boolean isActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(boolean activeStatus) {
    this.activeStatus = activeStatus;
  }

}
