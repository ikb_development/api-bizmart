package com.bizmart.auth.model.master.v1;

import com.bizmart.auth.common.BaseModel;
import java.io.Serializable;
import com.poiji.annotation.ExcelCell;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mst_supplier")
public class Supplier extends BaseModel implements Serializable {

  @Id
  @Column(name = "code")
  private String code = "";

  @Column(name = "name")
  private String name = "";

  @Column(name = "holdingCompanyCode")
  private String holdingCompanyCode = null;

  @Column(name = "companyIndustryCode")
  private String companyIndustryCode = null;

  @Column(name = "apAccountCode")
  private String apAccountCode = null;

  @Column(name = "paymentTermCode")
  private String paymentTermCode = null;

  @ExcelCell(5)
  @Column(name = "contactPerson")
  private String contactPerson = "";

  @ExcelCell(6)
  @Column(name = "email")
  private String email = "";

  @ExcelCell(7)
  @Column(name = "address")
  private String address = "";

  @ExcelCell(8)
  @Column(name = "zipCode")
  private String zipCode = "";

  @ExcelCell(9)
  @Column(name = "cityCode")
  private String cityCode = null;

  @ExcelCell(10)
  @Column(name = "countryCode")
  private String countryCode = null;

  @ExcelCell(11)
  @Column(name = "phone1")
  private String phone1 = "";

  @ExcelCell(12)
  @Column(name = "phone2")
  private String phone2 = "";

  @ExcelCell(13)
  @Column(name = "fax")
  private String fax = "";

  @ExcelCell(14)
  @Column(name = "npwp")
  private String npwp = "";

  @ExcelCell(15)
  @Column(name = "npwpName")
  private String npwpName = "";

  @ExcelCell(16)
  @Column(name = "npwpAddress")
  private String npwpAddress = "";

  @ExcelCell(17)
  @Column(name = "npwpZipCode")
  private String npwpZipCode = "";

  @ExcelCell(18)
  @Column(name = "npwpCityCode")
  private String npwpCityCode = null;

  @ExcelCell(19)
  @Column(name = "npwpCountryCode")
  private String npwpCountryCode = null;

  @ExcelCell(20)
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = false;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getHoldingCompanyCode() {
    return holdingCompanyCode;
  }

  public void setHoldingCompanyCode(String holdingCompanyCode) {
    this.holdingCompanyCode = holdingCompanyCode;
  }

  public String getCompanyIndustryCode() {
    return companyIndustryCode;
  }

  public void setCompanyIndustryCode(String companyIndustryCode) {
    this.companyIndustryCode = companyIndustryCode;
  }

  public String getApAccountCode() {
    return apAccountCode;
  }

  public void setApAccountCode(String apAccountCode) {
    this.apAccountCode = apAccountCode;
  }

  public String getPaymentTermCode() {
    return paymentTermCode;
  }

  public void setPaymentTermCode(String paymentTermCode) {
    this.paymentTermCode = paymentTermCode;
  }

  public String getContactPerson() {
    return contactPerson;
  }

  public void setContactPerson(String contactPerson) {
    this.contactPerson = contactPerson;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getCityCode() {
    return cityCode;
  }

  public void setCityCode(String cityCode) {
    this.cityCode = cityCode;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getPhone1() {
    return phone1;
  }

  public void setPhone1(String phone1) {
    this.phone1 = phone1;
  }

  public String getPhone2() {
    return phone2;
  }

  public void setPhone2(String phone2) {
    this.phone2 = phone2;
  }

  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public String getNpwp() {
    return npwp;
  }

  public void setNpwp(String npwp) {
    this.npwp = npwp;
  }

  public String getNpwpName() {
    return npwpName;
  }

  public void setNpwpName(String npwpName) {
    this.npwpName = npwpName;
  }

  public String getNpwpAddress() {
    return npwpAddress;
  }

  public void setNpwpAddress(String npwpAddress) {
    this.npwpAddress = npwpAddress;
  }

  public String getNpwpZipCode() {
    return npwpZipCode;
  }

  public void setNpwpZipCode(String npwpZipCode) {
    this.npwpZipCode = npwpZipCode;
  }

  public String getNpwpCityCode() {
    return npwpCityCode;
  }

  public void setNpwpCityCode(String npwpCityCode) {
    this.npwpCityCode = npwpCityCode;
  }

  public String getNpwpCountryCode() {
    return npwpCountryCode;
  }

  public void setNpwpCountryCode(String npwpCountryCode) {
    this.npwpCountryCode = npwpCountryCode;
  }

  public boolean isActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(boolean activeStatus) {
    this.activeStatus = activeStatus;
  }
}
