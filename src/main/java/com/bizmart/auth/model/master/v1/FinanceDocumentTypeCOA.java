/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizmart.auth.model.master.v1;

import com.bizmart.auth.common.BaseModel;
import com.poiji.annotation.ExcelCell;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author christ
 */
@Entity
@Table(name = "mst_document_type_chart_of_account")
public class FinanceDocumentTypeCOA extends BaseModel implements Serializable {

  @Id
  @ExcelCell(0)
  @Column(name = "Code")
  private String code = "";

  @ExcelCell(1)
  @Column(name = "financeDocumentTypeCode")
  private String financeDocumentTypeCode = null;

  @ExcelCell(1)
  @Column(name = "currencyCode")
  private String currencyCode = null;

  @ExcelCell(1)
  @Column(name = "chartOfAccountCode")
  private String chartOfAccountCode = null;

  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getFinanceDocumentTypeCode() {
    return financeDocumentTypeCode;
  }

  public void setFinanceDocumentTypeCode(String financeDocumentTypeCode) {
    this.financeDocumentTypeCode = financeDocumentTypeCode;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getChartOfAccountCode() {
    return chartOfAccountCode;
  }

  public void setChartOfAccountCode(String chartOfAccountCode) {
    this.chartOfAccountCode = chartOfAccountCode;
  }

  public boolean isActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(boolean activeStatus) {
    this.activeStatus = activeStatus;
  }
}
