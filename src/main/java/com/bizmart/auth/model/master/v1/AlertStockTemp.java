
package com.bizmart.auth.model.master.v1;

public class AlertStockTemp {
    private String itemCode="";
    private String blockNo="";
    private double onHandStock=0d;
    private double boStock=0d;
    private double soStock=0d;
    private double balance=0d;

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getBlockNo() {
        return blockNo;
    }

    public void setBlockNo(String blockNo) {
        this.blockNo = blockNo;
    }

    public double getOnHandStock() {
        return onHandStock;
    }

    public void setOnHandStock(double onHandStock) {
        this.onHandStock = onHandStock;
    }

    public double getBoStock() {
        return boStock;
    }

    public void setBoStock(double boStock) {
        this.boStock = boStock;
    }

    public double getSoStock() {
        return soStock;
    }

    public void setSoStock(double soStock) {
        this.soStock = soStock;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    
   
    
}
