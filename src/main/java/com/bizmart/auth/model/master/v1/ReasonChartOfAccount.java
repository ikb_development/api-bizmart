package com.bizmart.auth.model.master.v1;

import com.bizmart.auth.common.BaseModel;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mst_reason_chart_of_account")
public class ReasonChartOfAccount extends BaseModel implements Serializable {

  @Id
  @Column(name = "code")
  private String code = "";

  @Column(name = "name")
  private String name = "";

  @Column(name = "reasonCode")
  private String reasonCode = null;

  @Column(name = "branchCode")
  private String branchCode = null;

  @Column(name = "iinAccountCode")
  private String iinAccountCode = null;

  @Column(name = "iotAccountCode")
  private String iotAccountCode = null;

  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = false;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getReasonCode() {
    return reasonCode;
  }

  public void setReasonCode(String reasonCode) {
    this.reasonCode = reasonCode;
  }

  public String getBranchCode() {
    return branchCode;
  }

  public void setBranchCode(String branchCode) {
    this.branchCode = branchCode;
  }

  public String getIinAccountCode() {
    return iinAccountCode;
  }

  public void setIinAccountCode(String iinAccountCode) {
    this.iinAccountCode = iinAccountCode;
  }

  public String getIotAccountCode() {
    return iotAccountCode;
  }

  public void setIotAccountCode(String iotAccountCode) {
    this.iotAccountCode = iotAccountCode;
  }

  public boolean isActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(boolean activeStatus) {
    this.activeStatus = activeStatus;
  }
}
