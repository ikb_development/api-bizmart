package com.bizmart.auth.model.master.v1;

import com.bizmart.auth.common.BaseModel;
import java.io.Serializable;
import com.poiji.annotation.ExcelCell;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mst_exchangerate_tax")
public class ExchangerateTax extends BaseModel implements Serializable {

  @Id
  @ExcelCell(0)
  @Column(name = "Code")
  private String code = "";

  @ExcelCell(1)
  @Column(name = "currencyCode")
  private String currencyCode = null;

  @ExcelCell(2)
  @Column(name = "transactionDate")
  private Date transactionDate = new Date();

  @ExcelCell(2)
  @Column(name = "exchangeRate")
  private BigDecimal exchangeRate = new BigDecimal("0.00");

  @ExcelCell(3)
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = false;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public Date getTransactionDate() {
    return transactionDate;
  }

  public void setTransactionDate(Date transactionDate) {
    this.transactionDate = transactionDate;
  }

  public BigDecimal getExchangeRate() {
    return exchangeRate;
  }

  public void setExchangeRate(BigDecimal exchangeRate) {
    this.exchangeRate = exchangeRate;
  }

  public boolean isActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(boolean activeStatus) {
    this.activeStatus = activeStatus;
  }
}
