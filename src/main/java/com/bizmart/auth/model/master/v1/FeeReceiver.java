package com.bizmart.auth.model.master.v1;

import com.bizmart.auth.common.BaseModel;
import com.poiji.annotation.ExcelCell;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mst_fee_receiver")
public class FeeReceiver extends BaseModel implements Serializable {

  @Id
  @ExcelCell(0)
  @Column(name = "Code")
  private String code = "";

  @ExcelCell(1)
  @Column(name = "Name")
  private String name = "";

  @ExcelCell(2)
  @Column(name = "IdCardTypeCode")
  private String IdCardTypeCode = null;

  @ExcelCell(3)
  @Column(name = "idCardNo")
  private String idCardNo = "";

  @ExcelCell(4)
  @Column(name = "address")
  private String address = "";

  @ExcelCell(5)
  @Column(name = "zipCode")
  private String zipCode = "";

  @ExcelCell(6)
  @Column(name = "cityCode")
  private String cityCode = null;

  @ExcelCell(7)
  @Column(name = "countryCode")
  private String countryCode = null;

  @ExcelCell(8)
  @Column(name = "phone1")
  private String phone1 = "";

  @ExcelCell(9)
  @Column(name = "phone2")
  private String phone2 = "";

  @ExcelCell(10)
  @Column(name = "fax")
  private String fax = "";

  @ExcelCell(11)
  @Column(name = "contactPerson")
  private String contactPerson = "";

  @ExcelCell(12)
  @Column(name = "email")
  private String email = "";

  @ExcelCell(13)
  @Column(name = "feeReceiverTypeCode")
  private String feeReceiverTypeCode = null;

  @ExcelCell(14)
  @Column(name = "acNo")
  private String acNo = "";

  @ExcelCell(15)
  @Column(name = "acName")
  private String acName = "";

  @ExcelCell(16)
  @Column(name = "bankName")
  private String bankName = "";

  @ExcelCell(17)
  @Column(name = "bankBranch")
  private String bankBranch = "";

  @ExcelCell(18)
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = false;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getIdCardTypeCode() {
    return IdCardTypeCode;
  }

  public void setIdCardTypeCode(String IdCardTypeCode) {
    this.IdCardTypeCode = IdCardTypeCode;
  }

  public String getIdCardNo() {
    return idCardNo;
  }

  public void setIdCardNo(String idCardNo) {
    this.idCardNo = idCardNo;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getCityCode() {
    return cityCode;
  }

  public void setCityCode(String cityCode) {
    this.cityCode = cityCode;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getPhone1() {
    return phone1;
  }

  public void setPhone1(String phone1) {
    this.phone1 = phone1;
  }

  public String getPhone2() {
    return phone2;
  }

  public void setPhone2(String phone2) {
    this.phone2 = phone2;
  }

  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public String getContactPerson() {
    return contactPerson;
  }

  public void setContactPerson(String contactPerson) {
    this.contactPerson = contactPerson;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFeeReceiverTypeCode() {
    return feeReceiverTypeCode;
  }

  public void setFeeReceiverTypeCode(String feeReceiverTypeCode) {
    this.feeReceiverTypeCode = feeReceiverTypeCode;
  }

  public String getAcNo() {
    return acNo;
  }

  public void setAcNo(String acNo) {
    this.acNo = acNo;
  }

  public String getAcName() {
    return acName;
  }

  public void setAcName(String acName) {
    this.acName = acName;
  }

  public String getBankName() {
    return bankName;
  }

  public void setBankName(String bankName) {
    this.bankName = bankName;
  }

  public String getBankBranch() {
    return bankBranch;
  }

  public void setBankBranch(String bankBranch) {
    this.bankBranch = bankBranch;
  }

  public boolean isActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(boolean activeStatus) {
    this.activeStatus = activeStatus;
  }

}
