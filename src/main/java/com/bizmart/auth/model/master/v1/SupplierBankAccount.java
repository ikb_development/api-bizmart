package com.bizmart.auth.model.master.v1;

import com.bizmart.auth.common.BaseModel;
import com.poiji.annotation.ExcelCell;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mst_supplier_bank_account")
public class SupplierBankAccount extends BaseModel implements Serializable {

  @Id
  @Column(name = "code")
  private String code = "";

  @ExcelCell(1)
  @Column(name = "Name")
  private String name = "";

  @Column(name = "supplierCode")
  private String supplierCode = null;

  @ExcelCell(2)
  @Column(name = "acNo")
  private String acNo = "";

  @ExcelCell(3)
  @Column(name = "acName")
  private String acName = "";

  @ExcelCell(4)
  @Column(name = "bankName")
  private String bankName = "";

  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = false;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSupplierCode() {
    return supplierCode;
  }

  public void setSupplierCode(String supplierCode) {
    this.supplierCode = supplierCode;
  }

  public String getAcNo() {
    return acNo;
  }

  public void setAcNo(String acNo) {
    this.acNo = acNo;
  }

  public String getAcName() {
    return acName;
  }

  public void setAcName(String acName) {
    this.acName = acName;
  }

  public String getBankName() {
    return bankName;
  }

  public void setBankName(String bankName) {
    this.bankName = bankName;
  }

  public boolean isActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(boolean activeStatus) {
    this.activeStatus = activeStatus;
  }

}
