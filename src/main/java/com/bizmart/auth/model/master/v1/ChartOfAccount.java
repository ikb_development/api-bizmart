package com.bizmart.auth.model.master.v1;

import com.bizmart.auth.common.BaseModel;
import com.poiji.annotation.ExcelCell;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mst_chart_of_account")
public class ChartOfAccount extends BaseModel implements Serializable {

  @Id
  @ExcelCell(0)
  @Column(name = "Code")
  private String code = "";

  @ExcelCell(1)
  @Column(name = "Name")
  private String name = "";

  @ExcelCell(1)
  @Column(name = "accountType")
  private String accountType = "";

  @ExcelCell(1)
  @Column(name = "currencyCode")
  private String currencyCode = null;

  @ExcelCell(1)
  @Column(name = "iinStatus", columnDefinition = "TINYINT(1)")
  private boolean iinStatus = false;

  @ExcelCell(1)
  @Column(name = "iotStatus", columnDefinition = "TINYINT(1)")
  private boolean iotStatus = false;

  @ExcelCell(1)
  @Column(name = "bbmStatus", columnDefinition = "TINYINT(1)")
  private boolean bbmStatus = false;

  @ExcelCell(1)
  @Column(name = "bbkStatus", columnDefinition = "TINYINT(1)")
  private boolean bbkStatus = false;

  @ExcelCell(1)
  @Column(name = "bkmStatus", columnDefinition = "TINYINT(1)")
  private boolean bkmStatus = false;

  @ExcelCell(1)
  @Column(name = "bkkStatus", columnDefinition = "TINYINT(1)")
  private boolean bkkStatus = false;

  @ExcelCell(1)
  @Column(name = "gjmStatus", columnDefinition = "TINYINT(1)")
  private boolean gjmStatus = false;

  @ExcelCell(1)
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = false;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAccountType() {
    return accountType;
  }

  public void setAccountType(String accountType) {
    this.accountType = accountType;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public boolean isIinStatus() {
    return iinStatus;
  }

  public void setIinStatus(boolean iinStatus) {
    this.iinStatus = iinStatus;
  }

  public boolean isIotStatus() {
    return iotStatus;
  }

  public void setIotStatus(boolean iotStatus) {
    this.iotStatus = iotStatus;
  }

  public boolean isBbmStatus() {
    return bbmStatus;
  }

  public void setBbmStatus(boolean bbmStatus) {
    this.bbmStatus = bbmStatus;
  }

  public boolean isBbkStatus() {
    return bbkStatus;
  }

  public void setBbkStatus(boolean bbkStatus) {
    this.bbkStatus = bbkStatus;
  }

  public boolean isBkmStatus() {
    return bkmStatus;
  }

  public void setBkmStatus(boolean bkmStatus) {
    this.bkmStatus = bkmStatus;
  }

  public boolean isBkkStatus() {
    return bkkStatus;
  }

  public void setBkkStatus(boolean bkkStatus) {
    this.bkkStatus = bkkStatus;
  }

  public boolean isGjmStatus() {
    return gjmStatus;
  }

  public void setGjmStatus(boolean gjmStatus) {
    this.gjmStatus = gjmStatus;
  }

  public boolean isActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(boolean activeStatus) {
    this.activeStatus = activeStatus;
  }

}
