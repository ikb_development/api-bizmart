package com.bizmart.auth.model.master.v1;

import com.bizmart.auth.common.BaseModel;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name = "mst_barcode_detail")
public class BarcodeDetail extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code", unique = true, nullable = true, insertable = true, updatable = true)
  private String code = "";

  @Column(name = "itemCode")
  private Item itemCode = null;

  @Column(name = "itemPicturePath")
  private String itemPicturePath = "";

  @Column(name = "volume")
  private BigDecimal volume = new BigDecimal("0.0000");

  @Column(name = "supplier")
  private String supplier = null;

  @Column(name = "warehouseCode")
  private String warehouseCode = null;

  @Column(name = "itemStatusCode")
  private String itemStatusCode = null;

  @Column(name = "containerNo")
  private String containerNo = "";

  @Column(name = "blockNo")
  private String blockNo = "";

  @Column(name = "slabNo")
  private String slabNo = "";

  @Column(name = "locationCode")
  private String locationCode = null;

  @Column(name = "itemConditionCode")
  private String itemConditionCode = null;

  @Column(name = "remarkDate")
  private Date remarkDate = new Date();

  @Column(name = "remarkOther")
  private String remarkOther = "";

  @Column(name = "currencyCode")
  private String currencyCode = null;

  @Column(name = "exchangeRate")
  private BigDecimal exchangeRate = new BigDecimal("0.00");

  @Column(name = "cogsForeign")
  private BigDecimal cogsForeign = new BigDecimal("0.00");

  @Column(name = "cogsIdr")
  private BigDecimal cogsIdr = new BigDecimal("0.00");

  @Column(name = "itemCogs")
  private BigDecimal itemCogs = new BigDecimal("0.00");

  @Column(name = "additionalCogsIdr")
  private BigDecimal additionalCogsIdr = new BigDecimal("0.00");

  @Column(name = "postingStatus")
  private boolean postingStatus = false;

  @Column(name = "outStatus")
  private boolean outStatus = false;

  @Column(name = "inDate")
  private Date inDate = new Date();

  @Column(name = "inDocumentType")
  private String inDocumentType = "";

  @Column(name = "inDocumentNo")
  private String inDocumentNo = "";

  @Column(name = "outDate")
  private Date outDate = new Date();

  @Column(name = "outDocumentType")
  private String outDocumentType = "";

  @Column(name = "outDocumentNo")
  private String outDocumentNo = "";

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Item getItemCode() {
    return itemCode;
  }

  public void setItemCode(Item itemCode) {
    this.itemCode = itemCode;
  }

  public String getItemPicturePath() {
    return itemPicturePath;
  }

  public void setItemPicturePath(String itemPicturePath) {
    this.itemPicturePath = itemPicturePath;
  }

  public BigDecimal getVolume() {
    return volume;
  }

  public void setVolume(BigDecimal volume) {
    this.volume = volume;
  }

  public String getSupplier() {
    return supplier;
  }

  public void setSupplier(String supplier) {
    this.supplier = supplier;
  }

  public String getWarehouseCode() {
    return warehouseCode;
  }

  public void setWarehouseCode(String warehouseCode) {
    this.warehouseCode = warehouseCode;
  }

  public String getItemStatusCode() {
    return itemStatusCode;
  }

  public void setItemStatusCode(String itemStatusCode) {
    this.itemStatusCode = itemStatusCode;
  }

  public String getContainerNo() {
    return containerNo;
  }

  public void setContainerNo(String containerNo) {
    this.containerNo = containerNo;
  }

  public String getBlockNo() {
    return blockNo;
  }

  public void setBlockNo(String blockNo) {
    this.blockNo = blockNo;
  }

  public String getSlabNo() {
    return slabNo;
  }

  public void setSlabNo(String slabNo) {
    this.slabNo = slabNo;
  }

  public String getLocationCode() {
    return locationCode;
  }

  public void setLocationCode(String locationCode) {
    this.locationCode = locationCode;
  }

  public String getItemConditionCode() {
    return itemConditionCode;
  }

  public void setItemConditionCode(String itemConditionCode) {
    this.itemConditionCode = itemConditionCode;
  }

  public Date getRemarkDate() {
    return remarkDate;
  }

  public void setRemarkDate(Date remarkDate) {
    this.remarkDate = remarkDate;
  }

  public String getRemarkOther() {
    return remarkOther;
  }

  public void setRemarkOther(String remarkOther) {
    this.remarkOther = remarkOther;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public BigDecimal getExchangeRate() {
    return exchangeRate;
  }

  public void setExchangeRate(BigDecimal exchangeRate) {
    this.exchangeRate = exchangeRate;
  }

  public BigDecimal getCogsForeign() {
    return cogsForeign;
  }

  public void setCogsForeign(BigDecimal cogsForeign) {
    this.cogsForeign = cogsForeign;
  }

  public BigDecimal getCogsIdr() {
    return cogsIdr;
  }

  public void setCogsIdr(BigDecimal cogsIdr) {
    this.cogsIdr = cogsIdr;
  }

  public BigDecimal getItemCogs() {
    return itemCogs;
  }

  public void setItemCogs(BigDecimal itemCogs) {
    this.itemCogs = itemCogs;
  }

  public BigDecimal getAdditionalCogsIdr() {
    return additionalCogsIdr;
  }

  public void setAdditionalCogsIdr(BigDecimal additionalCogsIdr) {
    this.additionalCogsIdr = additionalCogsIdr;
  }

  public boolean isPostingStatus() {
    return postingStatus;
  }

  public void setPostingStatus(boolean postingStatus) {
    this.postingStatus = postingStatus;
  }

  public boolean isOutStatus() {
    return outStatus;
  }

  public void setOutStatus(boolean outStatus) {
    this.outStatus = outStatus;
  }

  public Date getInDate() {
    return inDate;
  }

  public void setInDate(Date inDate) {
    this.inDate = inDate;
  }

  public String getInDocumentType() {
    return inDocumentType;
  }

  public void setInDocumentType(String inDocumentType) {
    this.inDocumentType = inDocumentType;
  }

  public String getInDocumentNo() {
    return inDocumentNo;
  }

  public void setInDocumentNo(String inDocumentNo) {
    this.inDocumentNo = inDocumentNo;
  }

  public Date getOutDate() {
    return outDate;
  }

  public void setOutDate(Date outDate) {
    this.outDate = outDate;
  }

  public String getOutDocumentType() {
    return outDocumentType;
  }

  public void setOutDocumentType(String outDocumentType) {
    this.outDocumentType = outDocumentType;
  }

  public String getOutDocumentNo() {
    return outDocumentNo;
  }

  public void setOutDocumentNo(String outDocumentNo) {
    this.outDocumentNo = outDocumentNo;
  }

}
