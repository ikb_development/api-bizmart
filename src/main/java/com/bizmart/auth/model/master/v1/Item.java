package com.bizmart.auth.model.master.v1;

import com.bizmart.auth.common.BaseModel;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mst_item")
public class Item extends BaseModel implements Serializable {

  @Id
  @Column(name = "code")
  private String code = "";

  @Column(name = "name")
  private String name = "";

  @Column(name = "productTypeCode")
  private String productTypeCode = null;

  @Column(name = "productStatusCode")
  private String productStatusCode = null;

  @Column(name = "unitOfMeasureCode")
  private String unitOfMeasureCode = null;

  @Column(name = "thickness")
  private BigDecimal thickness = new BigDecimal("0.00");

  @Column(name = "cocurrencyCodede")
  private Currency currencyCode = null;

  @Column(name = "basedPrice")
  private BigDecimal basedPrice = new BigDecimal("0.00");

  @Column(name = "normalPrice")
  private BigDecimal normalPrice = new BigDecimal("0.00");

  @Column(name = "remark")
  private String remark = "";

  @Column(name = "salesperBlock")
  private boolean salesperBlock = false;

  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = false;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getProductTypeCode() {
    return productTypeCode;
  }

  public void setProductTypeCode(String productTypeCode) {
    this.productTypeCode = productTypeCode;
  }

  public String getProductStatusCode() {
    return productStatusCode;
  }

  public void setProductStatusCode(String productStatusCode) {
    this.productStatusCode = productStatusCode;
  }

  public String getUnitOfMeasureCode() {
    return unitOfMeasureCode;
  }

  public void setUnitOfMeasureCode(String unitOfMeasureCode) {
    this.unitOfMeasureCode = unitOfMeasureCode;
  }

  public BigDecimal getThickness() {
    return thickness;
  }

  public void setThickness(BigDecimal thickness) {
    this.thickness = thickness;
  }

  public Currency getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(Currency currencyCode) {
    this.currencyCode = currencyCode;
  }

  public BigDecimal getBasedPrice() {
    return basedPrice;
  }

  public void setBasedPrice(BigDecimal basedPrice) {
    this.basedPrice = basedPrice;
  }

  public BigDecimal getNormalPrice() {
    return normalPrice;
  }

  public void setNormalPrice(BigDecimal normalPrice) {
    this.normalPrice = normalPrice;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public boolean isSalesperBlock() {
    return salesperBlock;
  }

  public void setSalesperBlock(boolean salesperBlock) {
    this.salesperBlock = salesperBlock;
  }

  public boolean isActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(boolean activeStatus) {
    this.activeStatus = activeStatus;
  }
}
