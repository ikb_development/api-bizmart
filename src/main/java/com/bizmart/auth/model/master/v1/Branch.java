package com.bizmart.auth.model.master.v1;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.poiji.annotation.ExcelCell;

@Entity
@Table(name = "mst_branch")
public class Branch implements Serializable {

  @Id
  @ExcelCell(0)
  @Column(name = "Code")
  private String code = "";

  @ExcelCell(1)
  @Column(name = "Name")
  private String name = "";

  @ExcelCell(2)
  @Column(name = "logoPath")
  private String logoPath = "";

  @ExcelCell(3)
  @Column(name = "address")
  private String address = "";

  @ExcelCell(4)
  @Column(name = "cityCode")
  private String cityCode = null;

  @ExcelCell(5)
  @Column(name = "countryCode")
  private String countryCode = null;

  @ExcelCell(6)
  @Column(name = "phone1")
  private String phone1 = "";

  @ExcelCell(7)
  @Column(name = "phone2")
  private String phone2 = "";

  @ExcelCell(8)
  @Column(name = "fax")
  private String fax = "";

  @ExcelCell(9)
  @Column(name = "zipCode")
  private String zipCode = "";

  @ExcelCell(10)
  @Column(name = "npwp")
  private String npwp = "";

  @ExcelCell(11)
  @Column(name = "npwpAddress")
  private String npwpAddress = "";

  @ExcelCell(12)
  @Column(name = "contactPerson")
  private String contactPerson = "";

  @ExcelCell(13)
  @Column(name = "inventoryType")
  private String inventoryType;

  @ExcelCell(14)
  @Column(name = "warehouseCode")
  private String warehouseCode = null;

  @ExcelCell(15)
  @Column(name = "paymentTermCode")
  private String paymentTermCode = null;

  @ExcelCell(16)
  @Column(name = "customerCode")
  private String customerCode = null;

  @ExcelCell(17)
  @Column(name = "emailAddress")
  private String emailAddress = "";

  @ExcelCell(18)
  @Column(name = "independentBranchStatus")
  private String independentBranchStatus = "";

  @ExcelCell(19)
  @Column(name = "nppkpNo")
  private String nppkpNo = "";

  @ExcelCell(20)
  @Column(name = "taxInvoiceName")
  private String taxInvoiceName = "";

  @ExcelCell(21)
  @Column(name = "taxInvoiceLevel")
  private String taxInvoiceLevel = "";

  @ExcelCell(22)
  @Column(name = "cdpChartOfAccountCode")
  private String cdpChartOfAccountCode = "";

  @ExcelCell(23)
  @Column(name = "sdpChartOfAccountCode")
  private String sdpChartOfAccountCode = "";

  @ExcelCell(24)
  @Column(name = "vatAcronym")
  private String vatAcronym = "";

  @ExcelCell(25)
  @Column(name = "nonVatAcronym")
  private String nonVatAcronym = "";

  @ExcelCell(26)
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLogoPath() {
    return logoPath;
  }

  public void setLogoPath(String logoPath) {
    this.logoPath = logoPath;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getCityCode() {
    return cityCode;
  }

  public void setCityCode(String cityCode) {
    this.cityCode = cityCode;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getPhone1() {
    return phone1;
  }

  public void setPhone1(String phone1) {
    this.phone1 = phone1;
  }

  public String getPhone2() {
    return phone2;
  }

  public void setPhone2(String phone2) {
    this.phone2 = phone2;
  }

  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getNpwp() {
    return npwp;
  }

  public void setNpwp(String npwp) {
    this.npwp = npwp;
  }

  public String getNpwpAddress() {
    return npwpAddress;
  }

  public void setNpwpAddress(String npwpAddress) {
    this.npwpAddress = npwpAddress;
  }

  public String getContactPerson() {
    return contactPerson;
  }

  public void setContactPerson(String contactPerson) {
    this.contactPerson = contactPerson;
  }

  public String getInventoryType() {
    return inventoryType;
  }

  public void setInventoryType(String inventoryType) {
    this.inventoryType = inventoryType;
  }

  public String getWarehouseCode() {
    return warehouseCode;
  }

  public void setWarehouseCode(String warehouseCode) {
    this.warehouseCode = warehouseCode;
  }

  public String getPaymentTermCode() {
    return paymentTermCode;
  }

  public void setPaymentTermCode(String paymentTermCode) {
    this.paymentTermCode = paymentTermCode;
  }

  public String getCustomerCode() {
    return customerCode;
  }

  public void setCustomerCode(String customerCode) {
    this.customerCode = customerCode;
  }

  public String getEmailAddress() {
    return emailAddress;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public String getIndependentBranchStatus() {
    return independentBranchStatus;
  }

  public void setIndependentBranchStatus(String independentBranchStatus) {
    this.independentBranchStatus = independentBranchStatus;
  }

  public String getNppkpNo() {
    return nppkpNo;
  }

  public void setNppkpNo(String nppkpNo) {
    this.nppkpNo = nppkpNo;
  }

  public String getTaxInvoiceName() {
    return taxInvoiceName;
  }

  public void setTaxInvoiceName(String taxInvoiceName) {
    this.taxInvoiceName = taxInvoiceName;
  }

  public String getTaxInvoiceLevel() {
    return taxInvoiceLevel;
  }

  public void setTaxInvoiceLevel(String taxInvoiceLevel) {
    this.taxInvoiceLevel = taxInvoiceLevel;
  }

  public String getCdpChartOfAccountCode() {
    return cdpChartOfAccountCode;
  }

  public void setCdpChartOfAccountCode(String cdpChartOfAccountCode) {
    this.cdpChartOfAccountCode = cdpChartOfAccountCode;
  }

  public String getSdpChartOfAccountCode() {
    return sdpChartOfAccountCode;
  }

  public void setSdpChartOfAccountCode(String sdpChartOfAccountCode) {
    this.sdpChartOfAccountCode = sdpChartOfAccountCode;
  }

  public String getVatAcronym() {
    return vatAcronym;
  }

  public void setVatAcronym(String vatAcronym) {
    this.vatAcronym = vatAcronym;
  }

  public String getNonVatAcronym() {
    return nonVatAcronym;
  }

  public void setNonVatAcronym(String nonVatAcronym) {
    this.nonVatAcronym = nonVatAcronym;
  }

  public boolean isActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(boolean activeStatus) {
    this.activeStatus = activeStatus;
  }
}
