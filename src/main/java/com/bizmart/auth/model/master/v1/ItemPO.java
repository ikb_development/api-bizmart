
package com.bizmart.auth.model.master.v1;

/**
 *
 * This Model just for search item by PO Detail
 */
public class ItemPO {
    
    private String pod="";
    private String itemCode="";
    private String itemName="";
    private String itemUnit="";

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    
    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemUnit() {
        return itemUnit;
    }

    public void setItemUnit(String itemUnit) {
        this.itemUnit = itemUnit;
    }
    
    
    
}
