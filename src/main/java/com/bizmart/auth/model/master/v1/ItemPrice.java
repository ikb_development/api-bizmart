/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizmart.auth.model.master.v1;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author De4Ragil
 */
@Entity
@Table(name = "mst_item_price")
public class ItemPrice {

  @Id
  @Column(name = "Code")
  private String code = "";

  @Column(name = "itemCode")
  private String itemCode = null;

  @Column(name = "currencyCode")
  private String currencyCode = null;

  @Column(name = "basedPrice")
  private BigDecimal basedPrice = new BigDecimal(0.00);

  @Column(name = "normalPrice")
  private BigDecimal normalPrice = new BigDecimal(0.00);

  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = false;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public BigDecimal getBasedPrice() {
    return basedPrice;
  }

  public void setBasedPrice(BigDecimal basedPrice) {
    this.basedPrice = basedPrice;
  }

  public BigDecimal getNormalPrice() {
    return normalPrice;
  }

  public void setNormalPrice(BigDecimal normalPrice) {
    this.normalPrice = normalPrice;
  }

  public boolean isActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(boolean activeStatus) {
    this.activeStatus = activeStatus;
  }

}
