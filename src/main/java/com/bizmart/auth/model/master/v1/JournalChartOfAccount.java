package com.bizmart.auth.model.master.v1;

import com.bizmart.auth.common.BaseModel;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mst_journal_chart_of_account")
public class JournalChartOfAccount extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  private String code = "";

  @Column(name = "journalCode")
  private String journalCode = null;

  @Column(name = "currencyCode")
  private String currencyCode = null;

  @Column(name = "automaticJournalSetupCode")
  private String automaticJournalSetupCode = "";

  @Column(name = "chartOfAccountCode")
  private String chartOfAccountCode = null;

  @Column(name = "automaticJournalType")
  private String automaticJournalType = "";

  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getJournalCode() {
    return journalCode;
  }

  public void setJournalCode(String journalCode) {
    this.journalCode = journalCode;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getAutomaticJournalSetupCode() {
    return automaticJournalSetupCode;
  }

  public void setAutomaticJournalSetupCode(String automaticJournalSetupCode) {
    this.automaticJournalSetupCode = automaticJournalSetupCode;
  }

  public String getChartOfAccountCode() {
    return chartOfAccountCode;
  }

  public void setChartOfAccountCode(String chartOfAccountCode) {
    this.chartOfAccountCode = chartOfAccountCode;
  }

  public String getAutomaticJournalType() {
    return automaticJournalType;
  }

  public void setAutomaticJournalType(String automaticJournalType) {
    this.automaticJournalType = automaticJournalType;
  }

  public boolean isActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(boolean activeStatus) {
    this.activeStatus = activeStatus;
  }

}
