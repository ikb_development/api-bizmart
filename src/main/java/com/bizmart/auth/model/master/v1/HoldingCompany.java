package com.bizmart.auth.model.master.v1;

import com.bizmart.auth.common.BaseModel;
import com.poiji.annotation.ExcelCell;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mst_holding_company")
public class HoldingCompany extends BaseModel implements Serializable {

  @Id
  @ExcelCell(0)
  @Column(name = "Code")
  private String code = "";

  @ExcelCell(1)
  @Column(name = "Name")
  private String name = "";

  @ExcelCell(2)
  @Column(name = "companyIndustryCode")
  private String companyIndustryCode = null;

  @ExcelCell(2)
  @Column(name = "holdingCompanyCode")
  private BigDecimal creditLimit = new BigDecimal(0.0);

  @ExcelCell(4)
  @Column(name = "arAccount")
  private String arAccount = null;

  @ExcelCell(5)
  @Column(name = "contactPerson")
  private String contactPerson = "";

  @ExcelCell(6)
  @Column(name = "email")
  private String email = "";

  @ExcelCell(7)
  @Column(name = "address")
  private String address = "";

  @ExcelCell(8)
  @Column(name = "zipCode")
  private String zipCode = "";

  @ExcelCell(9)
  @Column(name = "cityCode")
  private String cityCode = null;

  @ExcelCell(10)
  @Column(name = "countryCode")
  private String countryCode = null;

  @ExcelCell(11)
  @Column(name = "phone1")
  private String phone1 = "";

  @ExcelCell(12)
  @Column(name = "phone2")
  private String phone2 = "";

  @ExcelCell(13)
  @Column(name = "fax")
  private String fax = "";

  @ExcelCell(14)
  @Column(name = "npwp")
  private String npwp = "";

  @ExcelCell(15)
  @Column(name = "npwpName")
  private String npwpName = "";

  @ExcelCell(16)
  @Column(name = "npwpAddress")
  private String npwpAddress = "";

  @ExcelCell(17)
  @Column(name = "npwpZipCode")
  private String npwpZipCode = "";

  @ExcelCell(18)
  @Column(name = "npwpCityCode")
  private String npwpCityCode = null;

  @ExcelCell(19)
  @Column(name = "npwpCountryCode")
  private String npwpCountryCode = null;

  @ExcelCell(20)
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = false;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCompanyIndustryCode() {
    return companyIndustryCode;
  }

  public void setCompanyIndustryCode(String companyIndustryCode) {
    this.companyIndustryCode = companyIndustryCode;
  }

  public BigDecimal getCreditLimit() {
    return creditLimit;
  }

  public void setCreditLimit(BigDecimal creditLimit) {
    this.creditLimit = creditLimit;
  }

  public String getArAccount() {
    return arAccount;
  }

  public void setArAccount(String arAccount) {
    this.arAccount = arAccount;
  }

  public String getContactPerson() {
    return contactPerson;
  }

  public void setContactPerson(String contactPerson) {
    this.contactPerson = contactPerson;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getCityCode() {
    return cityCode;
  }

  public void setCityCode(String cityCode) {
    this.cityCode = cityCode;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getPhone1() {
    return phone1;
  }

  public void setPhone1(String phone1) {
    this.phone1 = phone1;
  }

  public String getPhone2() {
    return phone2;
  }

  public void setPhone2(String phone2) {
    this.phone2 = phone2;
  }

  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public String getNpwp() {
    return npwp;
  }

  public void setNpwp(String npwp) {
    this.npwp = npwp;
  }

  public String getNpwpName() {
    return npwpName;
  }

  public void setNpwpName(String npwpName) {
    this.npwpName = npwpName;
  }

  public String getNpwpAddress() {
    return npwpAddress;
  }

  public void setNpwpAddress(String npwpAddress) {
    this.npwpAddress = npwpAddress;
  }

  public String getNpwpZipCode() {
    return npwpZipCode;
  }

  public void setNpwpZipCode(String npwpZipCode) {
    this.npwpZipCode = npwpZipCode;
  }

  public String getNpwpCityCode() {
    return npwpCityCode;
  }

  public void setNpwpCityCode(String npwpCityCode) {
    this.npwpCityCode = npwpCityCode;
  }

  public String getNpwpCountryCode() {
    return npwpCountryCode;
  }

  public void setNpwpCountryCode(String npwpCountryCode) {
    this.npwpCountryCode = npwpCountryCode;
  }

  public boolean isActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(boolean activeStatus) {
    this.activeStatus = activeStatus;
  }
  
}
