package com.bizmart.auth.service.v1;

import com.bizmart.auth.common.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.bizmart.auth.exception.v1.CustomException;
import com.bizmart.auth.model.v1.InvoiceHeader;
import com.bizmart.auth.model.v1.InvoiceDetail;
import com.bizmart.auth.repository.v1.InvoiceDetailRepository;
import com.bizmart.auth.repository.v1.InvoiceHeaderRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

@Service
public class SmlService {

  @Autowired
  private InvoiceHeaderRepository invoiceHeaderRepository;

  @Autowired
  private InvoiceDetailRepository invoiceDetailRepository;

  public InvoiceHeader save(MultipartFile file) {
    try {
      JSONObject obj = new JSONObject(Utility.fileJsonToMap(file));
      JSONArray objs = obj.getJSONArray("result");
      JSONObject rec = (JSONObject) objs.get(0);
      JSONArray questionsArray = rec.getJSONArray("prediction");
      InvoiceHeader invoiceHeader = new InvoiceHeader();
      List<InvoiceDetail> invoiceDetail = new ArrayList<>();
      for (int i = 0; i < questionsArray.length(); i++) {
        JSONObject recs = (JSONObject) questionsArray.get(i);
        if (recs.optString("label").equals("table")) {
          JSONArray arr = recs.getJSONArray("cells");
          invoiceDetail = detailList(arr);
        } else {
          if (recs.optString("label").equals("invoice_number")) {
            invoiceHeader.setCode(recs.optString("ocr_text"));
          }
          if (recs.optString("label").equals("nama_perusahaan")) {
            invoiceHeader.setCompanyName(recs.optString("ocr_text"));
          }
          if (recs.optString("label").equals("tax")) {
            invoiceHeader.setTax(recs.optString("ocr_text"));
          }
          if (recs.optString("label").equals("invoice_baseline")) {
            invoiceHeader.setBaseLine(recs.optString("ocr_text"));
          }
          if (recs.optString("label").equals("invoice_amount")) {
            invoiceHeader.setAmount(recs.optString("ocr_text"));
          }
          if (recs.optString("label").equals("invoice_date")) {
            invoiceHeader.setTransactionDate(recs.optString("ocr_text"));
          }
          if (recs.optString("label").equals("po_number")) {
            invoiceHeader.setPoNo(recs.optString("ocr_text"));
          }
          if (recs.optString("label").equals("invoice_amount_currecy")) {
            invoiceHeader.setCurrency(recs.optString("ocr_text"));
          }
        }
      }
      
      InvoiceHeader data = invoiceHeaderRepository.findByCode(invoiceHeader.getCode());
      if(data != null){
        throw new CustomException("The data already use!", HttpStatus.CONFLICT);
      }
      invoiceHeaderRepository.save(invoiceHeader);
      for (InvoiceDetail detail : invoiceDetail) {
        detail.setHeaderCode(invoiceHeader.getCode());
        invoiceDetailRepository.save(detail);
      }
      invoiceHeader.setInvoiceDetails(invoiceDetail);
      return invoiceHeader;
    } catch (IOException e) {
      throw new CustomException("The data error", HttpStatus.BAD_REQUEST);
    }
  }

  public InvoiceHeader search(String code) {
    InvoiceHeader data = invoiceHeaderRepository.findByCode(code);
    if (data == null) {
      throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
    }
    return data;
  }

  public List<InvoiceDetail> listDetail(String code) {
    List<InvoiceDetail> data = invoiceDetailRepository.findByHeaderCode(code);
    if (data.isEmpty()) {
      throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
    }
    return data;
  }

  private List<InvoiceDetail> detailList(JSONArray arr) {
    String prm = "List Detail:";
    List<InvoiceDetail> detail = new ArrayList<>();

    String[][] arrs = new String[3][4];

    for (int i = 0; i < arr.length(); i++) {
      JSONObject recs = (JSONObject) arr.get(i);

      int row = recs.optInt("row") - 1;
      int col = recs.optInt("col") - 1;
      arrs[row][col] = recs.optString("text");
    }

    for (int i = 0; i < arrs.length; i++) {
      if (arrs[i][0] != null) {
        InvoiceDetail invoiceDetail = new InvoiceDetail();
        invoiceDetail.setItemName(arrs[i][0]);
        invoiceDetail.setQuantity(arrs[i][1]);
        invoiceDetail.setAmount(arrs[i][2]);
        invoiceDetail.setSubTotal(arrs[i][3]);
        detail.add(invoiceDetail);
      }
    }
    return detail;
  }

}
