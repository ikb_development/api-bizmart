package com.bizmart.auth.service.master.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.bizmart.auth.exception.v1.CustomException;
import com.bizmart.auth.model.master.v1.Company;
import com.bizmart.auth.repository.master.v1.CompanyRepository;
import java.util.List;

@Service
public class CompanyService {

  @Autowired
  private CompanyRepository repo;

  public List<Company> list(String code, String name, boolean status) {
    try {
      List<Company> data = (List<Company>) repo.findAllBySearch(code, name, status);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
      }
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Company data(String code) {
    try {
      Company data = repo.findByCode(code);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
      }
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Company save(Company model) {
    try {
      Company data = repo.findByCode(model.getCode());
      if (data != null) {
        throw new CustomException("Data " + model.getCode() + " already use!", HttpStatus.CONFLICT);
      }
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Company update(Company model) {
    try {
      Company data = repo.findByCode(model.getCode());
      if (data == null) {
        throw new CustomException("Data " + model.getCode() + " can't update!", HttpStatus.CONFLICT);
      }
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Company delete(String code) {
    try {
      Company data = repo.findByCode(code);
      if (data != null) {
        throw new CustomException("Data " + code + "  Update!", HttpStatus.CONFLICT);
      }
      repo.deleteByCode(code);
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
