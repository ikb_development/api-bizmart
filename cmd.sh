mvn package
#docker rm $(docker stop $(docker ps -a -q --filter name="bizmart" --format="{{.ID}}"))
docker build -t "bizmart" .
docker run -i -d -p 80:8080 --name="bizmart" -v $PWD:/app "bizmart"
